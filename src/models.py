from sqlalchemy import (
    String,
    Float,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    UniqueConstraint,
    CheckConstraint,
    Boolean,
)
from sqlalchemy.ext.declarative import declarative_base
from core.db import engine


Base = declarative_base()


class Address(Base):
    __tablename__ = "addresses"

    id = Column(Integer, primary_key=True)
    region = Column(String(200), nullable=True)
    post_index = Column(String(10), nullable=True)
    city = Column(String(200), nullable=True)
    district = Column(String(200), nullable=True)
    street = Column(String(200), nullable=True)
    house_number = Column(String(200), nullable=True)
    apartment_number = Column(String(200), nullable=True)


class Debtor(Base):
    __tablename__ = "debtors"

    id = Column(Integer, primary_key=True)
    name = Column(String(200), nullable=False, unique=True)
    birth_date = Column(DateTime, nullable=False)
    birth_place = Column(String(300), nullable=False)
    inn = Column(String(50), unique=True)
    snils = Column(String(50), unique=True)
    address_id = Column(Integer, ForeignKey("addresses.id"), nullable=False)


class Bank(Base):
    __tablename__ = "banks"

    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False, unique=True)
    bik = Column(String(50), nullable=True, unique=True)


class Message(Base):
    __tablename__ = "messages"

    id = Column(Integer, primary_key=True)
    message_id = Column(String(100), nullable=False, unique=True)
    number = Column(String(50), nullable=False, unique=True)
    type = Column(String(100), nullable=False)
    publish_date = Column(DateTime, nullable=False)
    finish_reason = Column(String(500), nullable=True)
    debtor_id = Column(Integer, ForeignKey("debtors.id"), nullable=False)


class MessageBank(Base):
    __tablename__ = "messages_banks"

    id = Column(Integer, primary_key=True)
    message_id = Column(Integer, ForeignKey("messages.id"), nullable=False)
    bank_id = Column(Integer, ForeignKey("banks.id"), nullable=False)

    __table_args__ = (
        UniqueConstraint("message_id", "bank_id", name="uq_message_bank"),
    )


class ObligatoryPayment(Base):
    __tablename__ = "obligatory_payments"

    id = Column(Integer, primary_key=True)
    name = Column(String(500), nullable=False)
    amount = Column(Float, nullable=False)
    penalty = Column(Float, nullable=False, default=0)
    from_entrepreneurship = Column(Boolean, nullable=False, default=False)
    message_id = Column(Integer, ForeignKey("messages.id"), nullable=False)

    __table_args__ = (
        CheckConstraint("amount >= 0", name="check_obligatory_payment_amount"),
        CheckConstraint("penalty >= 0", name="check_obligatory_payment_penalty"),
    )


class MonetaryObligation(Base):
    __tablename__ = "monetary_obligations"

    id = Column(Integer, primary_key=True)
    creditor_name = Column(String(500), nullable=False)
    content = Column(String(500), nullable=False)
    basis = Column(String(500), nullable=False)

    total_sum = Column(Float, nullable=False)
    debt_sum = Column(Float, nullable=False)
    penalty = Column(Float, nullable=False, default=0)
    from_entrepreneurship = Column(Boolean, nullable=False, default=False)
    message_id = Column(Integer, ForeignKey("messages.id"), nullable=False)

    __table_args__ = (
        CheckConstraint("total_sum >= 0", name="check_monetary_obligation_total_sum"),
        CheckConstraint("debt_sum >= 0", name="check_monetary_obligation_debt_sum"),
        CheckConstraint("penalty >= 0", name="check_monetary_obligation_penalty"),
    )


if __name__ == "__main__":
    Base.metadata.create_all(engine)

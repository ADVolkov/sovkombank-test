SELECT_REGION_TOTAL_DEBT = """
select a.region, sum(case when mo.debt_sum is not null then mo.debt_sum else 0 end)::int total_debt
from debtors d
left join addresses a on a.id = d.address_id 
left join messages m on m.debtor_id = d.id 
left join monetary_obligations mo on mo.message_id = m.id 
where a.region is not null
group by a.region
order by total_debt desc
limit 5
"""

SELECT_DEBT_FOR_AGE = """
select 
concat((date_part('days', current_timestamp - d.birth_date) / 365)::int / 10, '0') debtor_age, 
sum(case when mo.debt_sum is not null then mo.debt_sum else 0 end)::int total_debt
from debtors d
left join messages m on m.debtor_id = d.id 
left join monetary_obligations mo on mo.message_id = m.id 
group by concat((date_part('days', current_timestamp - d.birth_date) / 365)::int / 10, '0')
order by debtor_age asc
"""

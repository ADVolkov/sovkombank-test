import re
from datetime import datetime

from sqlalchemy.orm import Session
from xml.etree.ElementTree import Element

from src.models import Bank, MonetaryObligation, ObligatoryPayment
from src.services import (
    get_or_create_address,
    get_or_create_debtor,
    get_or_create_message,
    get_or_create_banks,
    create_messagess_banks,
)


class XMLParser:
    def __init__(self, session: Session):
        self.session = session
        self.__monetary_obligations = []
        self.__obligatory_payments = []
        self.__regex_template = r"(\d{6})?\W*([\w-]+\s\w+)?\W*([\w-]+ р[айо-]{1,3}н)?\W*(\b(?:г[.\s]|р\.п|ст|гор|с\.|село|п\.|город|поселок|рп|пос|пгт|пр-?к?т?|д\.\s*[А-Я])\W*[\w\s-]*[\s,])?\W*(\b(?:ул|мкр\.|пер|улица)\W*[\w\d\s-]+[\s,])?\W*(\b(?:д\.|дом)\D*[\d\w/]+)?[\s,]*(\bкв.+)?"

    def parse_tree(self, data: dict, message: Element) -> None:
        for item in list(filter(lambda x: x.text is not None, message)):
            if item.tag in ("Bank", "MonetaryObligation", "ObligatoryPayment"):
                item.tag = len(data)
            data[item.tag] = item.text.strip() if len(item.text.strip()) > 0 else {}
            self.parse_tree(data[item.tag], item)

    @staticmethod
    def create_and_get_obligatory_payments_list(
        obligation: dict, message_id: int, from_entrepreneurship: bool
    ) -> list:
        return [
            ObligatoryPayment(
                name=data.get("Name"),
                amount=data.get("Sum"),
                penalty=data.get("PenaltySum", 0),
                from_entrepreneurship=from_entrepreneurship,
                message_id=message_id,
            )
            for obl_type, obligation in obligation
            for data in obligation.values()
            if obl_type == "ObligatoryPayments"
        ]

    @staticmethod
    def create_and_get_monetary_obligations_list(
        obligation: dict, message_id: int, from_entrepreneurship: bool
    ) -> list:
        return [
            MonetaryObligation(
                creditor_name=data.get("CreditorName"),
                content=data.get("Content"),
                basis=data.get("Basis"),
                total_sum=float(data.get("TotalSum")),
                debt_sum=float(data.get("DebtSum", 0)),
                penalty=float(data.get("PenaltySum", 0)),
                from_entrepreneurship=from_entrepreneurship,
                message_id=message_id,
            )
            for obl_type, obligation in obligation
            for data in obligation.values()
            if obl_type == "MonetaryObligations"
        ]

    def fill_database(self, xml_tree: Element) -> None:
        data: dict = {i: {} for i in range(len(xml_tree))}

        for message_id, message in enumerate(xml_tree):
            self.parse_tree(data[message_id], message)

        for msg in data.values():
            address = {
                i: addr
                for i, addr in zip(
                    (
                        "post_index",
                        "region",
                        "district",
                        "city",
                        "street",
                        "house_number",
                        "apart_number",
                    ),
                    (
                        j
                        for j in re.match(
                            self.__regex_template,
                            msg.get("Debtor").get("Address"),
                        ).groups()
                    ),
                )
            }
            if address.get("region"):
                if address.get("region").endswith("обл"):
                    address["region"] += "асть"
            new_addr = get_or_create_address(
                session=self.session,
                post_index=address.get("post_index"),
                region=address.get("region"),
                district=address.get("district"),
                city=address.get("city"),
                street=address.get("street"),
                house_number=address.get("house_number"),
                apartment_number=address.get("apart_number"),
            )

            new_debtor = get_or_create_debtor(
                session=self.session,
                name=msg.get("Debtor").get("Name"),
                birth_date=datetime.strptime(
                    msg.get("Debtor").get("BirthDate"), "%Y-%m-%dT%H:%M:%SZ"
                ),
                birth_place=msg.get("Debtor").get("BirthPlace"),
                inn=msg.get("Debtor").get("Inn", None),
                snils=msg.get("Debtor").get("Snils", None),
                address_id=new_addr.id,
            )
            new_msg = get_or_create_message(
                session=self.session,
                message_id=msg.get("Id"),
                number=msg.get("Number"),
                type=msg.get("Type"),
                publish_date=datetime.strptime(
                    msg["PublishDate"], "%Y-%m-%dT%H:%M:%S.%f"
                ),
                finish_reason=msg.get("FinishReason", None),
                debtor_id=new_debtor.id,
            )

            banks = (
                get_or_create_banks(
                    self.session,
                    [
                        Bank(name=bank.get("Name"), bik=bank.get("Bik"))
                        for i, bank in msg.get("Banks", None).items()
                    ],
                )
                if msg.get("Banks")
                else None
            )
            if banks:
                create_messagess_banks(self.session, banks, new_msg.id)

            if msg.get("CreditorsNonFromEntrepreneurship"):
                self.__monetary_obligations += (
                    self.create_and_get_monetary_obligations_list(
                        obligation=msg.get("CreditorsNonFromEntrepreneurship").items(),
                        message_id=new_msg.id,
                        from_entrepreneurship=False,
                    )
                )
                self.__obligatory_payments += (
                    self.create_and_get_obligatory_payments_list(
                        obligation=msg.get("CreditorsNonFromEntrepreneurship").items(),
                        message_id=new_msg.id,
                        from_entrepreneurship=False,
                    )
                )

            if msg.get("CreditorsFromEntrepreneurship"):
                self.__monetary_obligations += (
                    self.create_and_get_monetary_obligations_list(
                        obligation=msg.get("CreditorsFromEntrepreneurship").items(),
                        message_id=new_msg.id,
                        from_entrepreneurship=True,
                    )
                )
                self.__obligatory_payments += (
                    self.create_and_get_obligatory_payments_list(
                        obligation=msg.get("CreditorsNonFromEntrepreneurship").items(),
                        message_id=new_msg.id,
                        from_entrepreneurship=True,
                    )
                )
        if len(self.__obligatory_payments) > 0:
            self.session.add_all(self.__obligatory_payments)
            self.session.commit()
        if len(self.__monetary_obligations) > 0:
            self.session.add_all(self.__monetary_obligations)
            self.session.commit()

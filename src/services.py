import gzip
from datetime import datetime
from xml.etree import ElementTree
from xml.etree.ElementTree import Element

from sqlalchemy import or_
from sqlalchemy.orm import Session

from src.models import Address, Debtor, Message, Bank, MessageBank


def unzip_xml(filepath: str) -> Element:
    try:
        with gzip.open(filepath, "rb") as f:
            tree = ElementTree.parse(f)
            return tree.getroot()
    except FileNotFoundError:
        raise FileNotFoundError("Ошибка открытия файла")


def get_or_create_address(
    session: Session,
    post_index: str,
    region: str,
    district: str,
    city: str,
    street: str,
    house_number: str,
    apartment_number: str,
) -> Address:
    address = (
        session.query(Address)
        .filter(
            Address.post_index == post_index,
            Address.region == region,
            Address.district == district,
            Address.city == city,
            Address.street == street,
            Address.house_number == house_number,
            Address.apartment_number == apartment_number,
        )
        .first()
    )
    if not address:
        address = Address(
            post_index=post_index,
            region=region,
            district=district,
            city=city,
            street=street,
            house_number=house_number,
            apartment_number=apartment_number,
        )
        session.add(address)
        session.commit()
    return address


def get_or_create_debtor(
    session: Session,
    name: str,
    birth_date: datetime,
    birth_place: str,
    inn: str,
    snils: str,
    address_id: int,
) -> Debtor:
    debtor = session.query(Debtor).filter(Debtor.name == name).first()
    if not debtor:
        debtor = Debtor(
            name=name,
            birth_date=birth_date,
            birth_place=birth_place,
            inn=inn,
            snils=snils,
            address_id=address_id,
        )
        session.add(debtor)
        session.commit()
    return debtor


def get_or_create_message(
    session: Session,
    message_id: str,
    number: str,
    type: str,
    publish_date: datetime,
    finish_reason: str,
    debtor_id: int,
) -> Message:
    msg = session.query(Message).filter(Message.message_id == message_id).first()
    if not msg:
        msg = Message(
            message_id=message_id,
            number=number,
            type=type,
            publish_date=publish_date,
            finish_reason=finish_reason,
            debtor_id=debtor_id,
        )
        session.add(msg)
        session.commit()
    return msg


def get_or_create_banks(session: Session, banks: list) -> list:
    banks_to_add = []
    for i, bank in enumerate(banks):
        bank.bik = "" if not bank.bik else bank.bik
        get_bank = (
            session.query(Bank)
            .filter(or_(Bank.bik == bank.bik, Bank.name == bank.name))
            .first()
        )
        if not get_bank and [i.bik for i in banks if i].count(bank.bik) == 1:
            banks_to_add.append(bank)
        else:
            banks[i] = get_bank
    session.add_all(banks_to_add)
    session.commit()
    return banks


def create_messagess_banks(session: Session, banks: list, message_id: int):
    session.add_all(
        [MessageBank(message_id=message_id, bank_id=bank.id) for bank in banks if bank]
    )
    session.commit()


def bulk_update(session: Session, objects: list) -> None:
    try:
        session.add_all(objects)
        session.commit()
    except Exception:
        raise Exception

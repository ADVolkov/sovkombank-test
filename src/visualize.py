import matplotlib.pyplot as plt
import numpy as np

from src.core.db import get_connection
from src.sql_scripts import SELECT_REGION_TOTAL_DEBT, SELECT_DEBT_FOR_AGE


def create_region_total_debt_plot(conn):
    with conn.cursor() as cursor:
        cursor.execute(SELECT_REGION_TOTAL_DEBT)
        records = cursor.fetchall()
        plt.bar([rec[0] for rec in records], [rec[1] for rec in records])
        plt.xlabel("Регионы")
        plt.ylabel("Сумма задолженности")
        plt.title("Сумма задолженностей по регионам")
        plt.savefig("plots/region_debt.png")
    conn.close()


def create_debt_for_age_plot(conn):
    with conn.cursor() as cursor:
        cursor.execute(SELECT_DEBT_FOR_AGE)
        records = cursor.fetchall()
        plt.plot(
            np.array([rec[0] for rec in records]), np.array([rec[1] for rec in records])
        )
        plt.xlabel("Возраст")
        plt.ylabel("Сумма задолженности")
        plt.title("Сумма задолженностей по возрасту")
        plt.savefig("plots/age_range_debt.png")
    conn.close()


if __name__ == "__main__":
    create_region_total_debt_plot(conn=get_connection())
    # create_debt_for_age_plot(conn=get_connection())

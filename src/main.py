from src.core.config import FILE_NAME
from src.core.db import session

from src.parser import XMLParser
from src.services import unzip_xml


def main():
    XMLParser(session).fill_database(unzip_xml(f"files/{FILE_NAME}"))
    session.close()


if __name__ == "__main__":
    main()

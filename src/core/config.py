import os
import dotenv

dotenv.load_dotenv()


DB_USER = os.environ.get("DB_USER", "sovkombank")
DB_NAME = os.environ.get("DB_NAME", "sovkombank")
DB_PASSWORD = os.environ.get("DB_PASSWORD", "sovkombank")
DB_HOST = os.environ.get("DB_HOST", "localhost")
DB_PORT = os.environ.get("DB_PORT", "5432")
FILE_NAME = os.environ.get("FILE_NAME", "ExtrajudicialData.xml.gz")

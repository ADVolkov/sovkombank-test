from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session

from src.core.config import DB_USER, DB_PASSWORD, DB_NAME, DB_HOST, DB_PORT

import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


SQLALCHEMY_DATABASE_URL = (
    f"postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
)


def get_connection():
    connection = psycopg2.connect(
        dbname=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT
    )
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    return connection


def create_database():
    connection = psycopg2.connect(user=DB_USER, password=DB_PASSWORD)
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    cursor = connection.cursor()
    cursor.execute(f"CREATE DATABASE {DB_NAME}")

    cursor.close()
    connection.close()


engine = create_engine(SQLALCHEMY_DATABASE_URL)
Base = declarative_base()
session = Session(bind=engine)


if __name__ == "__main__":
    create_database()

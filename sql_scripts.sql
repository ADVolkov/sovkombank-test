-- 10 лиц с наибольшим количеством обязательств;

select d.name, d.inn, count(*) obl_cnt
from debtors d
	inner join messages m on d.id = m.debtor_id
	left join monetary_obligations mo on m.id = mo.message_id
group by d.name, d.inn
order by obl_cnt desc
limit 10

-- 10 лиц с наибольшей общей суммой задолженностей;

select d.name, d.inn, sum(case when mo.debt_sum is not null then mo.debt_sum else 0 end) total_sum
from debtors d
	inner join messages m on d.id = m.debtor_id
	left join monetary_obligations mo on m.id = mo.message_id
group by d.name, d.inn
order by total_sum desc
limit 10

-- всех физических лиц с процентом общей выплаченной суммы по всем обязательствам
-- относительно общей суммы своих обязательств (сколько процентов от всех обязательств погашено)
-- от меньшего к большему.

with dector_total_monetary(name, inn, total_monetary) as (
	select d.name, d.inn, sum(case when mo.debt_sum is not null then mo.debt_sum else 0 end) total_sum
	from debtors d
		inner join messages m on d.id = m.debtor_id
		left join monetary_obligations mo on m.id = mo.message_id
	group by d.name, d.inn
),
dector_total_payments(name, inn, total_payments) as (
	select d.name, d.inn, sum(case when op.amount is not null then op.amount else 0 end) total_payments
	from debtors d
		inner join messages m on d.id = m.debtor_id
		left join obligatory_payments op on op.message_id  = m.id
	group by d.name, d.inn
)
select dtp.name, dtp.inn, case when total_monetary > 0 then total_payments / total_monetary * 100 else 0 end repay_perc
from dector_total_payments dtp
	left join dector_total_monetary using(name, inn)
order by repay_perc asc
